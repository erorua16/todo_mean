////////////////////////////////////////////////////////////CONFIGURATION////////////////////////////////////////////////////////////

const express = require('express')
const app = express()
const mongoose = require('mongoose')
const Todo = require("./models/Todo")
const morgan = require('morgan')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const dotenv = require('dotenv')

dotenv.config();
const port = process.env.SERVER_PORT ||8179

mongoose.connect("mongodb+srv://<USERNAME>:<PASSWORD>@<CLUSTER>.mongodb.net/<DATABASENAME>?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }).then(() => {
    console.log("connection to database succeeded");
  })
  .catch((error) => {
    console.error("Error" + error.message);
  });

app.use(express.static(__dirname + "/public"))
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ 'extended': 'true'}))
app.use(bodyParser.json())
app.use(bodyParser.json({type: 'applres.send(err)ication/vnd.api+json'}))
app.use(methodOverride())




////////////////////////////////////////////////////////////API ROUTE CRUD FOR TODO////////////////////////////////////////////////////////////



//Get all in todo
app.get('/api/todos', (req, res) => {
    Todo.find((err, todos) => {
        if(err)
            res.send(err)
        res.json(todos)
    })
})


//Post todo
app.post('/api/todos', (req, res) => {
        //create a todo
        const todo = new Todo({
            text: req.body.text,
            person: req.body.person,
            done: false
        })
        
        todo.save((err, todo) => {
            if(err || !todo) {
                console.log(err)
            }
            Todo.find((err, todos) => {
                if(err){
                    console.log(err)
                }
                res.json(todos)
            })
        })
})


//Delete todo
app.delete('/api/todos/:todo_id', (req, res) => {
    //delete todo
    Todo.deleteOne({
        _id: req.params.todo_id
    }, (err, todo) => {
        if (err)
            res.send(err)


        // return list of all todos after deleting one
        Todo.find((err, todos) => {
            if(err)
                res.send(err)
            res.json(todos)
        })
    })
})

//Delete todo person
app.put('/api/todos/:todo_id', (req, res) => {
    //delete todo
    Todo.updateOne({
        _id: req.params.todo_id
    },{ $unset: {"person" : ""}}, (err, todo) => {
        if (err)
            res.send(err)

        // return list of all todos after updating one
        Todo.find((err, todos) => {
            if(err)
                res.send(err)
            res.json(todos)
        })
    })
})


////////////////////////////////////////////////////////////API ROUTE TO INDEX////////////////////////////////////////////////////////////



// Works sometimes, whatever i change server it stops working for some reason. I'm not sure i need it though 
app.get('*', (req, res) => {
    res.sendFile('./public/index.html')
})



////////////////////////////////////////////////////////////LISTEN////////////////////////////////////////////////////////////

app.listen(port, ()=>{
    console.log(`listening on http://localhost:${port}`);
})
