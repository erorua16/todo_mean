## Getting started

1. `npm i`
2. rename `.env_sample` to `.env`
3. change the mongoose.connect url to your mongodb account with your identifiers
change `<USERNAME>` to your username
change `<PASSWORD>` to your password
change `<DATABASENAME>` to the name of your database

## To run

1. open server with command `npm run dev`
2. You can delete a task or choose to only delete the assigned person, either one will make a confirmation screen pop up
3. You can add a task without assigning it to someone
4. You cannot add an assignement without a task name
5. To avoid duplicates, you cannot add a task with the same name as an already existing task
