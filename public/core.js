var scotchTodo = angular.module('scotchTodo', []);

function mainController($scope, $http){
    $scope.formData = {}

    //Show all current todos
    
    $http.get('/api/todos')
        .success((data)=> {
            $scope.todos = data
            console.log(data)
        })
        .error((data)=> {
            console.log('An error has occured:' + data)
        })
    
    //Send form info to API
    $scope.createTodo = () => {
        $http.post('/api/todos', $scope.formData)
            .success((data)=> {
                //reset form 
                $scope.formData = {}
                $scope.todos = data
                console.log(data)
            })
            .error((data)=>{
                console.log('An error has occured:' + data)
            })
    }

    //Delete a todo after checking it
    $scope.deleteTodo = (id, task) => {
        if(confirm("Are you sure to delete" + " " + task)) {
            $http.delete('/api/todos/' + id) 
                .success((data) => {
                    $scope.todos = data
                    console.log(data)
                })
                .error((data) => {
                    console.log('An error has occured:' + data)
                })
        }
    }

    //Delete assignment
    $scope.deleteTodoAssignment = (id, person) => {
        if(confirm("Are you sure to delete" + " " + person)) {
            $http.put('/api/todos/' + id) 
            .success((data) => {
                $scope.todos = data
                console.log(data)
            })
            .error((data) => {
                console.log('An error has occured:' + data)
            })
          }
    }
}