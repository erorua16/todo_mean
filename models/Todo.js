//////////////////////////////////////////////////////////////DATABASE MODEL////////////////////////////////////////////////////////////
const mongoose = require("mongoose");

var Todo = new mongoose.Schema(
    {
        text: {
            type: String, 
            unique: true,
            trim: true, 
            required: true,
        },
        person: {
            type: String,
            trim: true,
        }
    }
)

module.exports = mongoose.model("Todo", Todo);